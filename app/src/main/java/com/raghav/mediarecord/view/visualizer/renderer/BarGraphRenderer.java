/**
 * Copyright 2011, Felix Palmer
 * <p>
 * Licensed under the MIT license:
 * http://creativecommons.org/licenses/MIT/
 */
package com.raghav.mediarecord.view.visualizer.renderer;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import com.raghav.mediarecord.view.AudioData;
import com.raghav.mediarecord.view.FFTData;

public class BarGraphRenderer extends Renderer {
  private int divisions;
  private Paint paint;
  private boolean top;

  /**
   * Renders the FFT data as a series of lines, in histogram form
   * @param divisions - must be a power of 2. Controls how many lines to draw
   * @param paint - Paint to draw lines with
   * @param top - whether to draw the lines at the top of the canvas, or the bottom
   */
  public BarGraphRenderer(int divisions,
                          Paint paint,
                          boolean top) {
    super();
    this.divisions = divisions;
    this.paint = paint;
    this.top = top;
  }

  @Override
  public void onRender(Canvas canvas, AudioData data, Rect rect) {
    // Do nothing, we only display FFT data
  }

  @Override
  public void onRender(Canvas canvas, FFTData data, Rect rect) {
    for (int i = 0; i < data.bytes.length / divisions; i++) {
      FFTPoints[i * 4] = i * 4 * divisions;
      FFTPoints[i * 4 + 2] = i * 4 * divisions;
      byte rfk = data.bytes[divisions * i];
      byte ifk = data.bytes[divisions * i + 1];
      float magnitude = (rfk * rfk + ifk * ifk);
      int dbValue = (int) (30 * Math.log10(magnitude));

      if (top) {
        FFTPoints[i * 4 + 1] = 0;
        FFTPoints[i * 4 + 3] = (dbValue * 2 - 10);
      } else {
        FFTPoints[i * 4 + 1] = rect.height();
        FFTPoints[i * 4 + 3] = rect.height() - (dbValue * 2 - 10);
      }
    }

    canvas.drawLines(FFTPoints, paint);
  }
}
