/**
 * Copyright 2011, Felix Palmer
 * <p>
 * Licensed under the MIT license:
 * http://creativecommons.org/licenses/MIT/
 */
package com.raghav.mediarecord.view.visualizer.renderer;

import android.graphics.Canvas;
import android.graphics.Rect;

import com.raghav.mediarecord.view.AudioData;
import com.raghav.mediarecord.view.FFTData;


abstract public class Renderer {
  // Have these as members, so we don't have to re-create them each time
  protected float[] points;
  protected float[] FFTPoints;

  public Renderer() {
  }

  // As the display of raw/FFT audio will usually look different, subclasses
  // will typically only implement one of the below methods

  /**
   * Implement this method to render the audio data onto the canvas
   * @param canvas - Canvas to draw on
   * @param data - Data to render
   * @param rect - Rect to render into
   */
  abstract public void onRender(Canvas canvas, AudioData data, Rect rect);

  /**
   * Implement this method to render the FFT audio data onto the canvas
   * @param canvas - Canvas to draw on
   * @param data - Data to render
   * @param rect - Rect to render into
   */
  abstract public void onRender(Canvas canvas, FFTData data, Rect rect);


  // These methods should actually be called for rendering

  /**
   * Render the audio data onto the canvas
   * @param canvas - Canvas to draw on
   * @param data - Data to render
   * @param rect - Rect to render into
   */
  final public void render(Canvas canvas, AudioData data, Rect rect) {
    if (points == null || points.length < data.bytes.length * 4) {
      points = new float[data.bytes.length * 4];
    }

    onRender(canvas, data, rect);
  }

  /**
   * Render the FFT data onto the canvas
   * @param canvas - Canvas to draw on
   * @param data - Data to render
   * @param rect - Rect to render into
   */
  final public void render(Canvas canvas, FFTData data, Rect rect) {
    if (FFTPoints == null || FFTPoints.length < data.bytes.length * 4) {
      FFTPoints = new float[data.bytes.length * 4];
    }

    onRender(canvas, data, rect);
  }
}
