package com.raghav.mediarecord.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.SurfaceView;

import com.raghav.mediarecord.R;

import java.util.LinkedList;

public class WaveformView extends SurfaceView {

  // The number of buffer frames to keep around (for a nice fade-out visualization).
  private static final int HISTORY_SIZE = 6;
  private static final float MAX_AMPLITUDE_TO_DRAW = 8192f;

  // The queue that will hold historical audio data.
  private final LinkedList<short[]> audioData;

  private Paint backPaint;
  private Paint frontPaint;

  public WaveformView(Context context) {
    this(context, null, 0);
  }

  public WaveformView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public WaveformView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);

    audioData = new LinkedList<>();
    frontPaint = new Paint();
    frontPaint.setStyle(Paint.Style.STROKE);
    frontPaint.setStrokeWidth(0);
    frontPaint.setAntiAlias(true);
    backPaint = new Paint();
    backPaint.setAntiAlias(true);
//    backPaint.setARGB(255, 0, 0, 0);
    backPaint.setColor(context.getResources().getColor(R.color.dark_gray));
  }

  public class WaveFormThread extends Thread {
    boolean running = true;
    short[] buffer = null;

    @Override
    public void run() {
      while (running) {
        if (buffer != null) {
          updateAudioData();
        }
      }
    }

    public void setAudioData(short[] data) {
      buffer = data;
    }

    public void setRunning(boolean runFlag) {
      running = runFlag;
    }

    public void updateAudioData() {

      short[] newBuffer;
      // We want to keep a small amount of history in the view to provide a nice fading effect.
      // We use a linked list that we treat as a queue for this.
      if (audioData.size() == HISTORY_SIZE) {
        newBuffer = audioData.removeFirst();
        System.arraycopy(buffer, 0, newBuffer, 0, buffer.length);
      } else {
        newBuffer = buffer.clone();
      }

      audioData.addLast(newBuffer);

      // Update the display.
      Canvas canvas = getHolder().lockCanvas();
      if (canvas != null) {
        drawWaveform(canvas);
        getHolder().unlockCanvasAndPost(canvas);
      }
    }


    /**
     * Repaints the view's surface.
     *
     * @param canvas the {@link Canvas} object on which to draw
     */
    private void drawWaveform(Canvas canvas) {
      // Clear the screen each time because SurfaceView won't do this for us.
      canvas.drawPaint(backPaint);

      float width = getWidth();
      float height = getHeight();
      float centerY = height / 2;

      // We draw the history from oldest to newest so that the older audio data is further back
      // and darker than the most recent data.
      int colorDelta = 255 / (HISTORY_SIZE + 1);
      int brightness = colorDelta;

      for (short[] buffer : audioData) {
//        frontPaint.setColor(Color.argb(brightness, 128, 255, 192));
        frontPaint.setColor(Color.argb(brightness, 72, 255, 0));

        float lastX = -1;
        float lastY = -1;

        // For efficiency, we don't draw all of the samples in the buffer, but only the ones
        // that align with pixel boundaries.
        for (int x = 0; x < width; x++) {
          int index = (int) ((x / width) * buffer.length);
          short sample = buffer[index];
          float y = (sample / MAX_AMPLITUDE_TO_DRAW) * centerY + centerY;

          if (lastX != -1) {
            canvas.drawLine(lastX, lastY, x, y, frontPaint);
          }

          lastX = x;
          lastY = y;
        }

        brightness += colorDelta;
      }
    }
  }
}