/**
 * Copyright 2011, Felix Palmer
 * <p>
 * Licensed under the MIT license:
 * http://creativecommons.org/licenses/MIT/
 */
package com.raghav.mediarecord.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.media.audiofx.Visualizer;
import android.media.audiofx.Visualizer.OnDataCaptureListener;
import android.util.AttributeSet;
import android.view.View;

import com.raghav.mediarecord.view.visualizer.renderer.Renderer;

import java.util.HashSet;
import java.util.Set;

/**
 * A class that draws visualizations of data received from a
 * {@link OnDataCaptureListener#onWaveFormDataCapture } and
 * {@link OnDataCaptureListener#onFftDataCapture }
 */
public class VisualizerView extends View {
  private static final String TAG = "VisualizerView";
  boolean flash = false;
  Bitmap canvasBitmap;
  Canvas canvas;
  private byte[] dataBytes;
  private byte[] FFTBytes;
  private Rect rect = new Rect();
  private Visualizer visualizer;
  private Set<Renderer> renderers;
  private Paint flashPaint = new Paint();
  private Paint fadePaint = new Paint();

  public VisualizerView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs);
    init();
  }

  public VisualizerView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public VisualizerView(Context context) {
    this(context, null, 0);
  }

  @Override
  public boolean isInEditMode() {
    return true;
  }

  private void init() {
    dataBytes = null;
    FFTBytes = null;

    flashPaint.setColor(Color.argb(122, 255, 255, 255));
    fadePaint.setColor(Color.argb(238, 255, 255, 255)); // Adjust alpha to change how quickly the image fades
    fadePaint.setXfermode(new PorterDuffXfermode(Mode.MULTIPLY));

    renderers = new HashSet<Renderer>();
  }

  /**
   * Links the visualizer to a player
   *
   * @param player - MediaPlayer instance to link to
   */
  public void link(MediaPlayer player) {
    if (player == null) {
      throw new NullPointerException("Cannot link to null MediaPlayer");
    }

    // Create the Visualizer object and attach it to our media player.
    visualizer = new Visualizer(player.getAudioSessionId());
    visualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);

    // Pass through Visualizer data to VisualizerView
    OnDataCaptureListener captureListener = new OnDataCaptureListener() {
      @Override
      public void onWaveFormDataCapture(Visualizer visualizer, byte[] bytes,
                                        int samplingRate) {
        updateVisualizer(bytes);
      }

      @Override
      public void onFftDataCapture(Visualizer visualizer, byte[] bytes,
                                   int samplingRate) {
        updateVisualizerFFT(bytes);
      }
    };

    visualizer.setDataCaptureListener(captureListener,
        Visualizer.getMaxCaptureRate() / 2, true, true);

    visualizer.setEnabled(true);
  }

  public void addRenderer(Renderer renderer) {
    if (renderer != null) {
      renderers.add(renderer);
    }
  }

  public void clearRenderers() {
    renderers.clear();
  }

  /**
   * Call to release the resources used by VisualizerView. Like with the
   * MediaPlayer it is good practice to call this method
   */
  public void release() {
    visualizer.release();
  }

  /**
   * Pass data to the visualizer. Typically this will be obtained from the
   * Android Visualizer.OnDataCaptureListener call back. See
   * {@link OnDataCaptureListener#onWaveFormDataCapture }
   *
   * @param bytes
   */
  public void updateVisualizer(byte[] bytes) {
    dataBytes = bytes;
    invalidate();
  }

  /**
   * Pass FFT data to the visualizer. Typically this will be obtained from the
   * Android Visualizer.OnDataCaptureListener call back. See
   * {@link OnDataCaptureListener#onFftDataCapture }
   *
   * @param bytes
   */
  public void updateVisualizerFFT(byte[] bytes) {
    FFTBytes = bytes;
    invalidate();
  }

  /**
   * Call this to make the visualizer flash. Useful for flashing at the start
   * of a song/loop etc...
   */
  public void flash() {
    flash = true;
    invalidate();
  }

  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);

    // Create canvas once we're ready to draw
    rect.set(0, 0, getWidth(), getHeight());

    if (canvasBitmap == null) {
      canvasBitmap = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Config.ARGB_8888);
    }
    if (this.canvas == null) {
      this.canvas = new Canvas(canvasBitmap);
    }

    if (dataBytes != null) {
      // Render all audio renderers
      AudioData audioData = new AudioData(dataBytes);
      for (Renderer r : renderers) {
        r.render(this.canvas, audioData, rect);
      }
    }

    if (FFTBytes != null) {
      // Render all FFT renderers
      FFTData fftData = new FFTData(FFTBytes);
      for (Renderer r : renderers) {
        r.render(this.canvas, fftData, rect);
      }
    }

    // Fade out old contents
    this.canvas.drawPaint(fadePaint);

    if (flash) {
      flash = false;
      this.canvas.drawPaint(flashPaint);
    }

    canvas.drawBitmap(canvasBitmap, new Matrix(), null);
  }
}