package com.raghav.mediarecord;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.raghav.mediarecord.Adapters.CustomVideoAdapter;
import com.raghav.mediarecord.R;
import com.raghav.mediarecord.database.VideoDBSource;
import com.raghav.mediarecord.model.Video;

import java.util.ArrayList;

public class ShowVideoListActivity extends MonitorActivityChangeStatus implements AdapterView.OnItemClickListener {

  ArrayList<Video> arrayList;
  ListView listView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_recording_list);
    Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
    setSupportActionBar(toolbar);
    getWindow().setBackgroundDrawable(null);
    VideoDBSource db = new VideoDBSource(this);
    db.openDB();
    arrayList = db.readAllFromDB();
    listView = (ListView) findViewById(R.id.list);
    listView.setAdapter(new CustomVideoAdapter(this, arrayList));
    listView.setOnItemClickListener(this);
    db.closeDB();
  }

  @Override
  public void onItemClick(AdapterView l, View v, int position, long id) {
    Intent i = new Intent(this, PlayVideoActivity.class);
    i.putExtra("videoPath", arrayList.get(position).getVideoPath());
    startActivity(i);
  }
}
