package com.raghav.mediarecord.model;

public class Video {
  private String videoName;
  private String videoPath;
  private String videoDuration;
  private String videoSize;

  public Video() {
  }

  public String getVideoSize() {
    return videoSize;
  }

  public void setVideoSize(String videoSize) {
    this.videoSize = videoSize;
  }

  public String getVideoName() {
    return videoName;
  }

  public void setVideoName(String videoName) {
    this.videoName = videoName;
  }

  public String getVideoPath() {
    return videoPath;
  }

  public void setVideoPath(String videoPath) {
    this.videoPath = videoPath;
  }

  public String getVideoDuration() {
    return videoDuration;
  }

  public void setVideoDuration(String videoDuration) {
    this.videoDuration = videoDuration;
  }


    /*
    public videoBean(String videoName, String videoPath, String videoDuration, int videoSize) {
        this.videoName = videoName;
        this.videoPath = videoPath;
        this.videoDuration = videoDuration;
        this.videoSize = videoSize;
    }
    */
}
