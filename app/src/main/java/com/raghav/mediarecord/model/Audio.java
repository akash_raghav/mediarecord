package com.raghav.mediarecord.model;

public class Audio {
  private String audioName;
  private String audioPath;
  private String audioDuration;
  private long audioSize;

  public Audio() {
  }

    /*
    public AudioBean(String audioName, String audioPath, String audioDuration, int audioSize) {
        this.audioName = audioName;
        this.audioPath = audioPath;
        this.audioDuration = audioDuration;
        this.audioSize = audioSize;
    }
    */

  public String getAudioName() {
    return audioName;
  }

  public void setAudioName(String audioName) {
    this.audioName = audioName;
  }

  public String getAudioPath() {
    return audioPath;
  }

  public void setAudioPath(String audioPath) {
    this.audioPath = audioPath;
  }

  public String getAudioDuration() {
    return audioDuration;
  }

  public void setAudioDuration(String audioDuration) {
    this.audioDuration = audioDuration;
  }

  public long getAudioSize() {
    return audioSize;
  }

  public void setAudioSize(long audioSize) {
    this.audioSize = audioSize;
  }
}
