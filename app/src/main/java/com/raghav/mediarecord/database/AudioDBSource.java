package com.raghav.mediarecord.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.raghav.mediarecord.model.Audio;

import java.util.ArrayList;

public class AudioDBSource {

  public static final String LOG_TAG = "AudioRecording";

  AudioDBHelper helper;
  SQLiteDatabase db;

  public AudioDBSource(Context c) {
    helper = new AudioDBHelper(c);
  }

  public void openDB() {
    db = helper.getWritableDatabase();
  }

  public void closeDB() {
    db.close();
  }

  public void insertIntoDB(Audio audioBean) {
    ContentValues values = new ContentValues();

    values.put(AudioDBHelper.AUDIO_NAME, audioBean.getAudioName());
    values.put(AudioDBHelper.AUDIO_DURATION, audioBean.getAudioDuration());
    values.put(AudioDBHelper.AUDIO_PATH, audioBean.getAudioPath());
    values.put(AudioDBHelper.AUDIO_SIZE, audioBean.getAudioSize());

    db.insert(AudioDBHelper.TABLE_NAME, null, values);
    Log.d(LOG_TAG, "inserted to DB");
  }

    /*
    public void insertIntoDB(String audioName, String audioDuration, String audioPath, long audioSize) {
        ContentValues values = new ContentValues();

        values.put(AudioDatabaseHelper.AUDIO_NAME, audioName);
        values.put(AudioDatabaseHelper.AUDIO_DURATION, audioDuration);
        values.put(AudioDatabaseHelper.AUDIO_PATH, audioPath);
        values.put(AudioDatabaseHelper.AUDIO_SIZE, audioSize);

        db.insert(AudioDatabaseHelper.TABLE_NAME, null, values);
        Log.d(LOG_TAG, "inserted to DB");
    }
    */

  public ArrayList<Audio> readAllFromDB() {
    Log.d(LOG_TAG, "reading from DB");
    ArrayList<Audio> al = new ArrayList();
    Cursor cursor = db.rawQuery("SELECT * FROM " + AudioDBHelper.TABLE_NAME, null);
    Audio bean;

    while (cursor.moveToNext()) {
      bean = new Audio();
      bean.setAudioName(cursor.getString(cursor.getColumnIndex(AudioDBHelper.AUDIO_NAME)));
      bean.setAudioDuration(cursor.getString(cursor.getColumnIndex(AudioDBHelper.AUDIO_DURATION)));
      bean.setAudioPath(cursor.getString(cursor.getColumnIndex(AudioDBHelper.AUDIO_PATH)));
      bean.setAudioSize(cursor.getInt(cursor.getColumnIndex(AudioDBHelper.AUDIO_SIZE)));

      al.add(bean);
    }
    return al;
  }

  public String[] readColFromDB(String columnName) {
    Log.d(LOG_TAG, "reading from DB");
    Cursor cursor = db.rawQuery("SELECT " + columnName + " FROM " + AudioDBHelper.TABLE_NAME, null);
    String[] str = new String[cursor.getCount()];
    int i = 0;
    while (cursor.moveToNext()) {
      str[i] = cursor.getString(cursor.getColumnIndex(columnName));
    }
    return str;
  }
}