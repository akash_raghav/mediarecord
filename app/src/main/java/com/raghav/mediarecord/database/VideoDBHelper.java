package com.raghav.mediarecord.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class VideoDBHelper extends SQLiteOpenHelper {

  public static final String LOG_TAG = "VideoRecording";

  public static final String DATABASE_NAME = "VIDEO_DB";
  public static final String TABLE_NAME = "VIDEO_DATA";
  public static final String VIDEO_NAME = "VIDEO_NAME";
  public static final String VIDEO_DURATION = "VIDEO_DURATION";
  public static final String VIDEO_PATH = "VIDEO_PATH";
  public static final String VIDEO_SIZE = "VIDEO_SIZE";
  public static final int DB_VERSION = 1;

  public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME +
      "(" + VIDEO_NAME + " TEXT, " + VIDEO_DURATION + " TEXT, "
      + VIDEO_PATH + " TEXT, " + VIDEO_SIZE + " TEXT)";


  public VideoDBHelper(Context context) {
    super(context, DATABASE_NAME, null, DB_VERSION);
    Log.d(LOG_TAG, "DB opened or created");
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    db.execSQL(CREATE_TABLE);
    Log.d(LOG_TAG, "Table Created");
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    // work on it later
    Log.d(LOG_TAG, "Upgrade Called");
  }
}
