package com.raghav.mediarecord.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.raghav.mediarecord.model.Video;

import java.util.ArrayList;

public class VideoDBSource {

  public static final String LOG_TAG = "VideoRecording";

  VideoDBHelper helper;
  SQLiteDatabase db;

  public VideoDBSource(Context c) {
    helper = new VideoDBHelper(c);
  }

  public void openDB() {
    db = helper.getWritableDatabase();
  }

  public void closeDB() {
    db.close();
  }

  public void insertIntoDB(Video video) {
    ContentValues values = new ContentValues();

    values.put(VideoDBHelper.VIDEO_NAME, video.getVideoName());
    values.put(VideoDBHelper.VIDEO_DURATION, video.getVideoDuration());
    values.put(VideoDBHelper.VIDEO_PATH, video.getVideoPath());
    values.put(VideoDBHelper.VIDEO_SIZE, video.getVideoSize());

    db.insert(VideoDBHelper.TABLE_NAME, null, values);
    Log.d(LOG_TAG, "inserted to DB");
  }

    /*
    public void insertIntoDB(String videoName, String videoDuration, String videoPath, long videoSize) {
        ContentValues values = new ContentValues();

        values.put(VideoDatabaseHelper.VIDEO_NAME, videoName);
        values.put(VideoDatabaseHelper.VIDEO_DURATION, videoDuration);
        values.put(VideoDatabaseHelper.VIDEO_PATH, videoPath);
        values.put(VideoDatabaseHelper.VIDEO_SIZE, videoSize);

        db.insert(VideoDatabaseHelper.TABLE_NAME, null, values);
        Log.d(LOG_TAG, "inserted to DB");
    }
    */

  public ArrayList<Video> readAllFromDB() {
    Log.d(LOG_TAG, "reading from DB");
    ArrayList<Video> al = new ArrayList();
    Cursor cursor = db.rawQuery("SELECT * FROM " + VideoDBHelper.TABLE_NAME, null);
    Video bean;

    while (cursor.moveToNext()) {
      bean = new Video();
      bean.setVideoName(cursor.getString(cursor.getColumnIndex(VideoDBHelper.VIDEO_NAME)));
      bean.setVideoDuration(cursor.getString(cursor.getColumnIndex(VideoDBHelper.VIDEO_DURATION)));
      bean.setVideoPath(cursor.getString(cursor.getColumnIndex(VideoDBHelper.VIDEO_PATH)));
      bean.setVideoSize(cursor.getString(cursor.getColumnIndex(VideoDBHelper.VIDEO_SIZE)));

      al.add(bean);
    }
    return al;
  }

  public String[] readColFromDB(String columnName) {
    Log.d(LOG_TAG, "reading from DB");
    Cursor cursor = db.rawQuery("SELECT " + columnName + " FROM " + VideoDBHelper.TABLE_NAME, null);
    String[] str = new String[cursor.getCount()];
    int i = 0;
    while (cursor.moveToNext()) {
      str[i] = cursor.getString(cursor.getColumnIndex(columnName));
    }
    return str;
  }
}