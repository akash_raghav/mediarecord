package com.raghav.mediarecord.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class AudioDBHelper extends SQLiteOpenHelper {

  public static final String LOG_TAG = "AudioRecording";

  public static final String DATABASE_NAME = "Audio_DB";
  public static final String TABLE_NAME = "AUDIO_DATA";
  public static final String AUDIO_NAME = "AUDIO_NAME";
  public static final String AUDIO_DURATION = "AUDIO_DURATION";
  public static final String AUDIO_PATH = "AUDIO_PATH";
  public static final String AUDIO_SIZE = "AUDIO_SIZE";
  public static final int DB_VERSION = 1;

  public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME +
      "(" + AUDIO_NAME + " TEXT, " + AUDIO_DURATION + " TEXT, "
      + AUDIO_PATH + " TEXT, " + AUDIO_SIZE + " INTEGER)";


  public AudioDBHelper(Context context) {
    super(context, DATABASE_NAME, null, DB_VERSION);
    Log.d(LOG_TAG, "DB opened or created");
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    db.execSQL(CREATE_TABLE);
    Log.d(LOG_TAG, "Table Created");
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    // work on it later
    Log.d(LOG_TAG, "Upgrade Called");
  }
}
