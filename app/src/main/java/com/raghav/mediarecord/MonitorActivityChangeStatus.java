package com.raghav.mediarecord;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.widget.Toast;

  /* @author Akash Raghav

    assigning each method in the basic lifecycle of an activity unique ids
    1 - onCreate
    2 - onRestart
    3 - onStart
    4 - onResume
    5 - onPause
    6 - onStop
    7 - onDestroy

    and each operation has a fixed pattern in the lifecycle. considering the end point where the app is visible
    ============================================================================
    First Time activity start during app start          (1->3->4)

    Something like a dialog,etc came on Top             (5->4)

    moved to next activity                              (5->1->3->4->6)

    moved to previous activity                          (5->2->3->4->6->7)

    ============================================================================
    These are the conditions where app goes to background

    Another Activity from some other app came on top    (5->6-2->3->4)
    or home button pressed

    Back button pressed / Orientation changed           (5->6->7->1->3->4)
   */


public class MonitorActivityChangeStatus extends AppCompatActivity {

  private static final int ON_CREATE_ID = 1;
  private static final int ON_RESTART_ID = 2;
  private static final int ON_START_ID = 3;
  private static final int ON_RESUME_ID = 4;
  private static final int ON_PAUSE_ID = 5;
  private static final int ON_STOP_ID = 6;
  private static final int ON_DESTROY_ID = 7;

  private static final String FIRST_TIME_ACTIVITY_START = "134";
  private static final String DIALOG_CAME_ON_TOP_APP_VISIBLE = "54";
  private static final String MOVED_TO_NEXT_ACTIVITY = "51346";
  private static final String MOVED_TO_PREVIOUS_ACTIVITY = "523467";
  private static final String HOME_PRESSED_OR_OTHER_ACTIVITY_ON_TOP = "56234";
  private static final String BACK_PRESSED_OR_ORIENTATION_CHANGED = "567134";
  private static final StringBuilder stringBuilder = new StringBuilder();
  private static boolean backButtonPressed = false;
  private static int appInBackgroundCount = 0;
  private static Handler handler = new Handler();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    stringBuilder.append(ON_CREATE_ID);
  }

  @Override
  protected void onRestart() {
    super.onRestart();
    stringBuilder.append(ON_RESTART_ID);
  }

  @Override
  protected void onStart() {
    super.onStart();
    stringBuilder.append(ON_START_ID);
  }

  @Override
  protected void onResume() {
    super.onResume();
    stringBuilder.append(ON_RESUME_ID);
    handler.postDelayed(new Runnable() {
      @Override
      public void run() {
        if (checkIfAppWasInBackground()) {
          appInBackgroundCount++;
          if (appInBackgroundCount >= 5) {
            Toast.makeText(MonitorActivityChangeStatus.this, "Disqualified for breaking the code of conduct. closing test.", Toast.LENGTH_SHORT).show();
            finish();
          }
        }
      }
    }, 500);
  }

  @Override
  protected void onPause() {
    super.onPause();
    stringBuilder.append(ON_PAUSE_ID);
  }

  @Override
  protected void onStop() {
    super.onStop();
    stringBuilder.append(ON_STOP_ID);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    stringBuilder.append(ON_DESTROY_ID);
  }

  boolean checkIfAppWasInBackground() {
    boolean appBackgroundStatus = false;

    if (stringBuilder.toString().equals(FIRST_TIME_ACTIVITY_START)) {
      Toast.makeText(MonitorActivityChangeStatus.this, "First time activity start", Toast.LENGTH_SHORT).show();
    } else if (stringBuilder.toString().equals(DIALOG_CAME_ON_TOP_APP_VISIBLE)) {
      Toast.makeText(MonitorActivityChangeStatus.this, "Dialog came on top", Toast.LENGTH_SHORT).show();
    } else if (stringBuilder.toString().equals(MOVED_TO_NEXT_ACTIVITY)) {
      Toast.makeText(MonitorActivityChangeStatus.this, "Moved to next activity", Toast.LENGTH_SHORT).show();
    } else if (stringBuilder.toString().equals(MOVED_TO_PREVIOUS_ACTIVITY)) {
      Toast.makeText(MonitorActivityChangeStatus.this, "Moved to previous activity", Toast.LENGTH_SHORT).show();
      backButtonPressed = false;
    } else if (stringBuilder.toString().equals(HOME_PRESSED_OR_OTHER_ACTIVITY_ON_TOP)) {
      Toast.makeText(MonitorActivityChangeStatus.this, "Monitored: App was in background", Toast.LENGTH_SHORT).show();
      appBackgroundStatus = true;
    } else if (stringBuilder.toString().equals(BACK_PRESSED_OR_ORIENTATION_CHANGED)) {
      String text;
      if (backButtonPressed) {
        text = "Monitored: App was in background";
        backButtonPressed = false;
        appBackgroundStatus = true;
      } else {
        text = "Orientation Changed";
      }
      Toast.makeText(MonitorActivityChangeStatus.this, text, Toast.LENGTH_SHORT).show();
    } else {
      Toast.makeText(MonitorActivityChangeStatus.this, "unknown cycle : " + stringBuilder, Toast.LENGTH_SHORT).show();
    }
    stringBuilder.delete(0, stringBuilder.length());
    return appBackgroundStatus;
  }

  @Override
  public boolean onKeyDown(int keyCode, KeyEvent event) {
    if (keyCode == KeyEvent.KEYCODE_BACK) {
      backButtonPressed = true;
    }
    return super.onKeyDown(keyCode, event);
  }
}