package com.raghav.mediarecord;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.raghav.mediarecord.Adapters.CustomAudioAdapter;
import com.raghav.mediarecord.R;
import com.raghav.mediarecord.database.AudioDBSource;
import com.raghav.mediarecord.model.Audio;

import java.util.ArrayList;

public class ShowAudioListActivity extends MonitorActivityChangeStatus implements AdapterView.OnItemClickListener {

  ArrayList<Audio> arrayList;
  ListView listView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_recording_list);
    Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
    setSupportActionBar(toolbar);
    getWindow().setBackgroundDrawable(null);
    AudioDBSource db = new AudioDBSource(this);
    db.openDB();
    arrayList = db.readAllFromDB();
    listView = (ListView) findViewById(R.id.list);
    listView.setAdapter(new CustomAudioAdapter(this, arrayList));
    listView.setOnItemClickListener(this);
    db.closeDB();
  }

  @Override
  public void onItemClick(AdapterView l, View v, final int position, long id) {
    Intent i = new Intent(ShowAudioListActivity.this, PlayAudioActivity.class);
    i.putExtra("PATH", arrayList.get(position).getAudioPath());
    startActivity(i);
  }

}