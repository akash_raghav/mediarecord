package com.raghav.mediarecord.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.raghav.mediarecord.R;
import com.raghav.mediarecord.PlayVideoActivity;
import com.raghav.mediarecord.model.Video;

import java.util.ArrayList;

public class CustomVideoAdapter extends BaseAdapter {

  ArrayList<Video> videoBeanList;
  Context context;

  public CustomVideoAdapter(Context context, ArrayList<Video> videoBeanList) {
    this.context = context;
    this.videoBeanList = videoBeanList;
  }

  @Override
  public int getCount() {
    return videoBeanList.size();
  }

  @Override
  public Object getItem(int position) {
    return position;
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    if (convertView == null) {
      LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      convertView = inflater.inflate(R.layout.adapter_custom_layout, parent, false);
    }

    final Video bean = videoBeanList.get(position);

    TextView tv1 = (TextView) convertView.findViewById(R.id.textView1);
    TextView tv2 = (TextView) convertView.findViewById(R.id.textView2);
    ImageView iv = (ImageView) convertView.findViewById(R.id.imageButton);

    iv.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent i = new Intent(context, PlayVideoActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra("videoPath", bean.getVideoPath());
        context.startActivity(i);
      }
    });
    tv1.setText(position + 1 + ".");
    tv2.setText(bean.getVideoName());

    return convertView;
  }
}
