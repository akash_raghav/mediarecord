package com.raghav.mediarecord.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.raghav.mediarecord.R;
import com.raghav.mediarecord.PlayAudioActivity;
import com.raghav.mediarecord.model.Audio;

import java.util.ArrayList;

public class CustomAudioAdapter extends BaseAdapter {

  ArrayList<Audio> audioList;
  Context context;

  public CustomAudioAdapter(Context context, ArrayList<Audio> audioList) {
    this.context = context;
    this.audioList = audioList;
  }

  @Override
  public int getCount() {
    return audioList.size();
  }

  @Override
  public Object getItem(int position) {
    return position;
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    if (convertView == null) {
      LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      convertView = inflater.inflate(R.layout.adapter_custom_layout, parent, false);
    }

    final Audio bean = audioList.get(position);

    TextView tv1 = (TextView) convertView.findViewById(R.id.textView1);
    TextView tv2 = (TextView) convertView.findViewById(R.id.textView2);
    ImageView iv = (ImageView) convertView.findViewById(R.id.imageButton);

    iv.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent i = new Intent(context, PlayAudioActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra("PATH", bean.getAudioPath());
        context.startActivity(i);
      }
    });
    tv1.setText(String.valueOf(position + 1));
    tv2.setText(bean.getAudioName());

    return convertView;
  }
}
