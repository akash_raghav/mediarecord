package com.raghav.mediarecord;

import android.content.Intent;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.media.CamcorderProfile;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.widget.Toolbar;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.raghav.mediarecord.R;
import com.raghav.mediarecord.database.VideoDBSource;
import com.raghav.mediarecord.model.Video;
import com.raghav.mediarecord.utils.AMLog;

import java.io.File;
import java.io.IOException;

public class RecordingVideoActivity extends MonitorActivityChangeStatus {

  private static final String VIDEO_RECORDER_FILE_EXT_MP4 = ".mp4";
  private static final String VIDEO_RECORDER_FOLDER = "/VideoRecorder";
  private static final int AUDIO_RECORDER_SOURCE = MediaRecorder.AudioSource.MIC;
  private static final int VIDEO_RECORDER_SOURCE = MediaRecorder.VideoSource.CAMERA;
  private RecordingThread recordingThread;
  private boolean isRecording;
  private MediaRecorder mediaRecorder;
  private VideoDBSource videoDBSource;
  private Video video;
  private Camera camera;
  private CameraInfo info = new CameraInfo();
  private MediaPlayer mediaPlayer;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_video_recording);
    Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
    setSupportActionBar(toolbar);
    getWindow().setBackgroundDrawable(null);

    Button startButton = (Button) findViewById(R.id.start_recording);
    Button stopButton = (Button) findViewById(R.id.stop_recording);
    Button playButton = (Button) findViewById(R.id.play_recording);
    Button showRecordings = (Button) findViewById(R.id.show_recordings);

    startButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        enableButtons(true);
        if (!isRecording && camera != null) {
          isRecording = true;
          recordingThread = new RecordingThread();
          recordingThread.start();
          changeView(true);
        }
      }
    });

    stopButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        AMLog.d("stop clicked");
        enableButtons(false);
        if (isRecording) {
          recordingThread.setRunning(false);
          int i = 0;
          while (i == 0) {
            try {
              recordingThread.join();
              i = 1;
            } catch (InterruptedException e) {
              AMLog.e(e);
            }
          }
          changeView(false);
          recordingThread = null;
        } else if (mediaPlayer != null && mediaPlayer.isPlaying()) {
          mediaPlayer.stop();
          mediaPlayer.reset();
          mediaPlayer.release();
          mediaPlayer = null;
          changeView(false);
        }
      }
    });

    playButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        playRecording();
      }
    });

    showRecordings.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(RecordingVideoActivity.this, ShowVideoListActivity.class));
      }
    });

    enableButtons(false);
    playButton.setEnabled(false);

    videoDBSource = new VideoDBSource(this);
    video = new Video();
  }

  void initFrontCamera() {
    int cameraId;
    int numCameras = Camera.getNumberOfCameras();
    for (cameraId = 0; cameraId < numCameras; cameraId++) {
      Camera.getCameraInfo(cameraId, info);
      if (CameraInfo.CAMERA_FACING_FRONT == info.facing) {
        break;
      }
    }

    try {
      camera = null;
      camera = Camera.open(cameraId);

      int rotation = getWindowManager().getDefaultDisplay().getRotation();
      int degrees = 0;

      switch (rotation) {
        case Surface.ROTATION_0:
          degrees = 0;
          break;
        case Surface.ROTATION_90:
          degrees = 90;
          break;
        case Surface.ROTATION_180:
          degrees = 180;
          break;
        case Surface.ROTATION_270:
          degrees = 270;
          break;
      }

      int result;
      result = (info.orientation + degrees) % 360;
      result = (360 - result) % 360;  // compensate the mirror
      camera.setDisplayOrientation(result);

    } catch (Exception e) {
      AMLog.e(e);
    }
  }


  void initRecorder() {
    camera.unlock();
    mediaRecorder = new MediaRecorder();
    mediaRecorder.setCamera(camera);
    mediaRecorder.setAudioSource(AUDIO_RECORDER_SOURCE);
    mediaRecorder.setVideoSource(VIDEO_RECORDER_SOURCE);

    CamcorderProfile camcorderProfile = CamcorderProfile.get(CamcorderProfile.QUALITY_480P);
    mediaRecorder.setProfile(camcorderProfile);

    mediaRecorder.setOutputFile(getFileName());
    mediaRecorder.setOrientationHint(info.orientation);
    mediaRecorder.setPreviewDisplay(((SurfaceView) findViewById(R.id.display_surface)).getHolder().getSurface());

    try {
      mediaRecorder.prepare();
    } catch (IOException e) {
      AMLog.e(e);
    }
    mediaRecorder.start();
  }


  void releaseRecorder() {
    if (mediaRecorder != null && isRecording) {
      AMLog.d("trying to release camera");
      try {
        camera.reconnect();
        AMLog.d("camera stopped");
        AMLog.d("trying to release recorder");
        mediaRecorder.stop();
        AMLog.d("recorder stopped");
        mediaRecorder.reset();
        mediaRecorder.release();
        AMLog.d("recorder released");
        makeDB();
      } catch (RuntimeException e) {
        AMLog.e(e);
        File f = new File(video.getVideoPath());
        if (f.exists()) {
          f.delete();
        }
        video.setVideoPath(null);
        video.setVideoName("No video data received due to very short capture span");
      } catch (IOException e) {
        AMLog.e(e);
      }
      camera.stopPreview();
      mediaRecorder = null;
      isRecording = false;
    }
  }

  void releaseCamera() {
    camera.lock();
    camera.release();
    camera = null;
  }


  void makeDB() {
    File videoFile = new File(video.getVideoPath());
    long lengthInBytes = videoFile.length();
    double lengthInKB = lengthInBytes / 1024;
    video.setVideoSize(String.valueOf(lengthInKB));

    MediaPlayer mediaPlayer = MediaPlayer.create(this, Uri.fromFile(videoFile));
    video.setVideoDuration((mediaPlayer.getDuration() / 1000) + "");
    mediaPlayer.reset();
    mediaPlayer.release();
    videoDBSource.insertIntoDB(video);
  }

  private void playRecording() {
    changeView(true);
    enableButtons(true);
    findViewById(R.id.play_recording).setVisibility(View.GONE);
    findViewById(R.id.show_recordings).setVisibility(View.GONE);
    SurfaceView surfaceView = (SurfaceView) findViewById(R.id.display_surface);
    mediaPlayer = MediaPlayer.create(this, Uri.fromFile(new File(video.getVideoPath())), surfaceView.getHolder());

    mediaPlayer.start();
    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
      @Override
      public void onCompletion(MediaPlayer mp) {
        mediaPlayer.stop();
        mediaPlayer.reset();
        mediaPlayer.release();
        enableButtons(false);
        changeView(false);
      }
    });
  }

  @Override
  protected void onResume() {
    super.onResume();
    videoDBSource.openDB();
    initFrontCamera();
  }

  @Override
  protected void onPause() {
    super.onPause();
    videoDBSource.closeDB();
    releaseCamera();
  }

  private void changeView(boolean displaySurface) {

    if (displaySurface) {
      findViewById(R.id.display_surface).setVisibility(View.VISIBLE);
      findViewById(R.id.data_view).setVisibility(View.GONE);
      findViewById(R.id.play_recording).setVisibility(View.GONE);
      findViewById(R.id.show_recordings).setVisibility(View.GONE);
    } else {
      findViewById(R.id.display_surface).setVisibility(View.GONE);
      View dataView = findViewById(R.id.data_view);
      ((TextView) dataView.findViewById(R.id.video_name)).setText(getString(R.string.file_name) + video.getVideoName());
      ((TextView) dataView.findViewById(R.id.video_path)).setText(getString(R.string.file_path) + video.getVideoPath());
      ((TextView) dataView.findViewById(R.id.video_duration)).setText(getString(R.string.media_duration) + video.getVideoDuration() + " sec.");
      ((TextView) dataView.findViewById(R.id.video_size)).setText(getString(R.string.media_size) + video.getVideoSize() + " KB");
      dataView.setVisibility(View.VISIBLE);
      findViewById(R.id.play_recording).setVisibility(View.VISIBLE);
      findViewById(R.id.show_recordings).setVisibility(View.VISIBLE);
    }

  }

  private String getFileName() {
    String state = Environment.getExternalStorageState();
    String videoFilePath;
    if (Environment.MEDIA_MOUNTED.equals(state)) {
      videoFilePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES).getPath();
    } else {
      videoFilePath = getFilesDir().getPath();
    }

    File videoFolderPath = new File(videoFilePath, VIDEO_RECORDER_FOLDER);

    if (!videoFolderPath.exists()) {
      if (!videoFolderPath.mkdirs()) {
        AMLog.e(VIDEO_RECORDER_FOLDER + " folder creation failed");
      }
    }

    videoFilePath = (videoFolderPath.getAbsolutePath() + "/" + System.currentTimeMillis() + VIDEO_RECORDER_FILE_EXT_MP4);
    video.setVideoPath(videoFilePath);
    video.setVideoName(new File(videoFilePath).getName());
    return videoFilePath;
  }

  void enableButtons(boolean isRecording) {
    findViewById(R.id.start_recording).setEnabled(!isRecording);
    findViewById(R.id.stop_recording).setEnabled(isRecording);
    findViewById(R.id.play_recording).setEnabled(!isRecording);
    findViewById(R.id.show_recordings).setEnabled(!isRecording);
  }

  class RecordingThread extends Thread {
    boolean running = true;

    public boolean getRunning() {
      return this.running;
    }

    public void setRunning(boolean running) {
      this.running = running;
    }

    @Override
    public void run() {
      initRecorder();
      while (getRunning()) ;
      releaseRecorder();
    }
  }
}