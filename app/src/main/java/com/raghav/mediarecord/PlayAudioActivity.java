package com.raghav.mediarecord;

import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;

import com.raghav.mediarecord.R;
import com.raghav.mediarecord.view.VisualizerView;
import com.raghav.mediarecord.view.visualizer.renderer.BarGraphRenderer;

import java.io.File;

public class PlayAudioActivity extends MonitorActivityChangeStatus {

  private ImageView playPauseButton;
  private SeekBar seekBar;
  private MediaPlayer mediaPlayer;
  private VisualizerView visualizerView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_play_audio);
    Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
    setSupportActionBar(toolbar);
    getWindow().setBackgroundDrawable(null);

    playPauseButton = (ImageView) findViewById(R.id.playPauseButton);
    playPauseButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
          mediaPlayer.pause();
          playPauseButton.setBackgroundResource(R.drawable.play_button);
        } else {
          if (mediaPlayer == null) {
            init();
          }
        }
      }
    });
    seekBar = (SeekBar) findViewById(R.id.seekBar);
    visualizerView = (VisualizerView) findViewById(R.id.visualizerView);
  }

  @Override
  protected void onResume() {
    super.onResume();
    init();
  }

  @Override
  protected void onPause() {
    cleanUp();
    super.onPause();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
  }

  private void init() {
    File audioFile = new File(getIntent().getStringExtra("PATH"));
    mediaPlayer = MediaPlayer.create(this, Uri.fromFile(audioFile));
    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
      @Override
      public void onCompletion(MediaPlayer mp) {
        cleanUp();
        visualizerView.setEnabled(false);
      }
    });
    mediaPlayer.start();


//    playPauseButton.setBackgroundResource(R.drawable.pause);
    setUpWithSeekBar();


    // We need to link the visualizer view to the media player so that
    // it displays something
    visualizerView.link(mediaPlayer);

    Paint paint = new Paint();
    paint.setStrokeWidth(50f);
    paint.setAntiAlias(true);
    paint.setColor(Color.argb(200, 56, 138, 252));
    BarGraphRenderer barGraphRendererBottom = new BarGraphRenderer(16, paint, false);
    visualizerView.addRenderer(barGraphRendererBottom);

  }

  private void cleanUp() {
    if (mediaPlayer != null) {
      visualizerView.release();
      mediaPlayer.stop();
      mediaPlayer.reset();
      mediaPlayer.release();
      mediaPlayer = null;
      seekBar.setProgress(0);
      playPauseButton.setBackgroundResource(R.drawable.play_button);
    }
  }

  void setUpWithSeekBar() {
    seekBar.setMax(mediaPlayer.getDuration());

    seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

      @Override
      public void onStopTrackingTouch(SeekBar seekBar) {
      }

      @Override
      public void onStartTrackingTouch(SeekBar seekBar) {
      }

      @Override
      public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (mediaPlayer != null && fromUser) {
          mediaPlayer.seekTo(progress);
        }
      }
    });

    final Handler handler;
    handler = new Handler();
    //Make sure you update Seekbar on UI thread
    runOnUiThread(new Runnable() {

      @Override
      public void run() {
        if (mediaPlayer != null) {
          int mCurrentPosition = mediaPlayer.getCurrentPosition();
          seekBar.setProgress(mCurrentPosition);
        }
        handler.postDelayed(this, 1000);
      }
    });

  }

}
