package com.raghav.mediarecord;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.raghav.mediarecord.database.AudioDBSource;
import com.raghav.mediarecord.model.Audio;
import com.raghav.mediarecord.utils.AMLog;
import com.raghav.mediarecord.view.VisualizerView;
import com.raghav.mediarecord.view.WaveformView;
import com.raghav.mediarecord.view.visualizer.renderer.BarGraphRenderer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class RecordingAudioActivity extends MonitorActivityChangeStatus {

  private static final int RECORDER_BPP = 16;
  private static final int RECORDER_SAMPLE_RATE = 44100;
  private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
  private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
  private static final String AUDIO_RECORDER_FILE_EXT_WAV = ".wav";
  private static final String AUDIO_RECORDER_FOLDER = "/AudioRecorder";
  private static final String AUDIO_RECORDER_TEMP_FILE = "record_temp.raw";

  private MediaPlayer mediaPlayer;
  private AudioRecord recorder;
  private Thread recordingThread;
  private int bufferSize;
  private boolean isRecording;
  private File currentRecordedTrack;
  private AudioDBSource db;
  private Audio bean;

  private Button startButton;
  private Button stopButton;
  private Button playButton;
  private Button showListButton;
  private WaveformView waveformView;
  private WaveformView.WaveFormThread waveFormThread;
  private VisualizerView visualizerView;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_audio_recording);
    Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
    setSupportActionBar(toolbar);
    getWindow().setBackgroundDrawable(null);

    db = new AudioDBSource(this);
    bufferSize = AudioRecord.getMinBufferSize(RECORDER_SAMPLE_RATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING);

    startButton = (Button) findViewById(R.id.startButton);
    stopButton = (Button) findViewById(R.id.stopButton);
    playButton = (Button) findViewById(R.id.playButton);
    showListButton = (Button) findViewById(R.id.showListButton);

    waveformView = (WaveformView) findViewById(R.id.waveform_view);
    visualizerView = (VisualizerView) findViewById(R.id.visualizerView);

    startButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        enableButtons(true);
        startRecording();
      }
    });

    stopButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        enableButtons(false);
        stopRecording();
      }
    });

    playButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        enableButtons(true);
        playRecording();
      }
    });

    showListButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(new Intent(RecordingAudioActivity.this, ShowAudioListActivity.class));
      }
    });

    enableButtons(false);
    playButton.setVisibility(View.GONE);

  }

  @Override
  protected void onResume() {
    super.onResume();
    db.openDB();
  }

  @Override
  protected void onPause() {
    super.onPause();
    db.closeDB();
  }

  void enableButtons(boolean isRecording) {
    if (isRecording) {
      startButton.setVisibility(View.GONE);
      stopButton.setVisibility(View.VISIBLE);
      playButton.setVisibility(View.GONE);
      showListButton.setVisibility(View.GONE);
      waveformView.setVisibility(View.VISIBLE);
    } else {
      startButton.setVisibility(View.VISIBLE);
      stopButton.setVisibility(View.GONE);
      playButton.setVisibility(View.VISIBLE);
      showListButton.setVisibility(View.VISIBLE);
      waveformView.setVisibility(View.GONE);
    }
  }

  void playRecording() {
    mediaPlayer = MediaPlayer.create(this, Uri.fromFile(currentRecordedTrack));
    isRecording = false;

    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
      @Override
      public void onCompletion(MediaPlayer mp) {
        cleanUp();
        enableButtons(false);
      }
    });

    mediaPlayer.start();
    visualizerView.setVisibility(View.VISIBLE);
    visualizerView.link(mediaPlayer);

    Paint paint = new Paint();
    paint.setStrokeWidth(50f);
    paint.setAntiAlias(true);
    paint.setColor(Color.argb(200, 56, 138, 252));
    BarGraphRenderer barGraphRendererBottom = new BarGraphRenderer(16, paint, false);
    visualizerView.addRenderer(barGraphRendererBottom);
  }

  private void cleanUp() {
    if (mediaPlayer != null) {
      visualizerView.release();
      mediaPlayer.stop();
      visualizerView.setVisibility(View.GONE);
      mediaPlayer.reset();
      mediaPlayer.release();
      mediaPlayer = null;
    }
  }

  public void vibrate(int duration) {
    Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
    vibrator.vibrate(duration);
  }

  void startRecording() {

    recordingThread = new Thread("AudioRecorder Thread") {

      @Override
      public void run() {
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO);
        bean = new Audio();
        recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
            RECORDER_SAMPLE_RATE, RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING, bufferSize);

        if (recorder.getState() == AudioRecord.STATE_INITIALIZED) {
          recorder.startRecording();
          waveFormThread = waveformView.new WaveFormThread();
          isRecording = true;
          writeAudioDataToFile();
        }
      }
    };

    recordingThread.start();

  }

  private void writeAudioDataToFile() {
    byte[] byteArray = new byte[bufferSize];
    short[] shortArray = new short[bufferSize / 2];
    ByteBuffer byteBuffer = ByteBuffer.allocateDirect(bufferSize);
    byteBuffer.order(ByteOrder.LITTLE_ENDIAN);

    String filename = getTempFilename();
    FileOutputStream os = null;

    try {
      os = new FileOutputStream(filename);
    } catch (FileNotFoundException e) {
      AMLog.e(e);
    }

    int audioReadState;

    if (os != null) {
      waveFormThread.start();
      while (isRecording) {
        audioReadState = recorder.read(shortArray, 0, bufferSize / 2);
        waveFormThread.setAudioData(shortArray);
        byteBuffer.asShortBuffer().put(shortArray);
        byteBuffer.get(byteArray);
        byteBuffer.clear();

        if (audioReadState != AudioRecord.ERROR_INVALID_OPERATION) {
          try {
            os.write(byteArray);
          } catch (IOException e) {
            AMLog.e(e);
          }
        }
      }

      waveFormThread.setRunning(false);

      int i = 1;
      while (i == 1) {
        try {
          waveFormThread.join();
          recordingThread.join();
          i = 0;
        } catch (InterruptedException e) {
          AMLog.e(e);
        }
      }

      waveFormThread = null;
      recordingThread = null;

      try {
        os.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  private void stopRecording() {
    if (recorder != null && isRecording) {
      isRecording = false;

      if (recorder.getState() == AudioRecord.STATE_INITIALIZED) {
        recorder.stop();
        recorder.release();
        recorder = null;
      }

      currentRecordedTrack = new File(getFilename());
      copyWaveFile(getTempFilename(), currentRecordedTrack);
      deleteTempFile();

      bean.setAudioName(currentRecordedTrack.getName());
      bean.setAudioPath(currentRecordedTrack.getAbsolutePath());

      int durationStr;
      mediaPlayer = MediaPlayer.create(this, Uri.fromFile(currentRecordedTrack));
      durationStr = mediaPlayer.getDuration();
      mediaPlayer.reset();
      mediaPlayer.release();
      AMLog.d("duration : " + durationStr / 1000);

      long length = currentRecordedTrack.length();
      length = length / 1024;

      bean.setAudioDuration(durationStr / 1000 + "");
      bean.setAudioSize((int) length);
      AMLog.d("size : " + length + " kb");

      db.insertIntoDB(bean);
    } else if ((mediaPlayer != null) && (mediaPlayer.isPlaying())) {
      cleanUp();
    }

  }

  private void deleteTempFile() {
    File file = new File(getTempFilename());
    AMLog.v("temp file deleted : " + file.delete());
  }


  private void copyWaveFile(String inFilename, File outFilename) {
    FileInputStream in;
    FileOutputStream out;
    long totalAudioLen;
    long totalDataLen;
    long longSampleRate = RECORDER_SAMPLE_RATE;
    int channels = 1;
    long byteRate = RECORDER_BPP * longSampleRate * channels / 8;

    byte[] data = new byte[bufferSize];

    try {
      in = new FileInputStream(inFilename);
      out = new FileOutputStream(outFilename);
      totalAudioLen = in.getChannel().size();
      totalDataLen = totalAudioLen + 36;

      Log.d("AudioRecording", "File size: " + totalDataLen);

      WriteWaveFileHeader(out, totalAudioLen, totalDataLen,
          longSampleRate, channels, byteRate);

      while (in.read(data) != -1) {
        out.write(data);
      }

      in.close();
      out.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private void WriteWaveFileHeader(
      FileOutputStream out, long totalAudioLen,
      long totalDataLen, long longSampleRate, int channels,
      long byteRate) throws IOException {

    byte[] header = new byte[44];

    // RIFF/WAVE header
    header[0] = 'R';
    header[1] = 'I';
    header[2] = 'F';
    header[3] = 'F';

    //file size
    header[4] = (byte) (totalDataLen & 0xff);
    header[5] = (byte) ((totalDataLen >> 8) & 0xff);
    header[6] = (byte) ((totalDataLen >> 16) & 0xff);
    header[7] = (byte) ((totalDataLen >> 24) & 0xff);

    //WAVE
    header[8] = 'W';
    header[9] = 'A';
    header[10] = 'V';
    header[11] = 'E';

    header[12] = 'f'; // 'fmt ' chunk
    header[13] = 'm';
    header[14] = 't';
    header[15] = ' ';

    header[16] = 16; // 4 bytes: size of 'fmt ' chunk
    header[17] = 0;
    header[18] = 0;
    header[19] = 0;

    header[20] = 1; // format = 1
    header[21] = 0;

    header[22] = (byte) channels;
    header[23] = 0;

    header[24] = (byte) (longSampleRate & 0xff);
    header[25] = (byte) ((longSampleRate >> 8) & 0xff);
    header[26] = (byte) ((longSampleRate >> 16) & 0xff);
    header[27] = (byte) ((longSampleRate >> 24) & 0xff);

    header[28] = (byte) (byteRate & 0xff);
    header[29] = (byte) ((byteRate >> 8) & 0xff);
    header[30] = (byte) ((byteRate >> 16) & 0xff);
    header[31] = (byte) ((byteRate >> 24) & 0xff);

    header[32] = (byte) (RECORDER_BPP * channels / 8); // block align
    header[33] = 0;

    header[34] = RECORDER_BPP; // bits per sample
    header[35] = 0;

    header[36] = 'd';
    header[37] = 'a';
    header[38] = 't';
    header[39] = 'a';

    header[40] = (byte) (totalAudioLen & 0xff);
    header[41] = (byte) ((totalAudioLen >> 8) & 0xff);
    header[42] = (byte) ((totalAudioLen >> 16) & 0xff);
    header[43] = (byte) ((totalAudioLen >> 24) & 0xff);

    out.write(header, 0, 44);
  }

  private String getFilename() {
    String filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).getPath();
    File file = new File(filePath, AUDIO_RECORDER_FOLDER);

    if (!file.exists()) {
      AMLog.v("audio directory created : " + file.mkdirs());
    }

    return (file.getAbsolutePath() + "/" + System.currentTimeMillis() + AUDIO_RECORDER_FILE_EXT_WAV);
  }

  private String getTempFilename() {
    String filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC).getPath();
    File file = new File(filePath, AUDIO_RECORDER_FOLDER);

    if (!file.exists()) {
      AMLog.v("audio directory created : " + file.mkdirs());
    }

    File tempFile = new File(filePath, AUDIO_RECORDER_TEMP_FILE);

    if (tempFile.exists()) {
      AMLog.v("temp file deleted : " + tempFile.delete());
    }

    return (file.getAbsolutePath() + "/" + AUDIO_RECORDER_TEMP_FILE);
  }

}