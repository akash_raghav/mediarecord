package com.raghav.mediarecord;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.MediaController;
import android.widget.VideoView;

import com.raghav.mediarecord.R;

public class PlayVideoActivity extends MonitorActivityChangeStatus {

  VideoView videoView;
  MediaController mediaController;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_video_player);
    Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
    setSupportActionBar(toolbar);
    getWindow().setBackgroundDrawable(null);

    videoView = (VideoView) findViewById(R.id.videoView);
    mediaController = new MediaController(this);
    videoView.setMediaController(mediaController);
    videoView.setVideoPath(getIntent().getStringExtra("videoPath"));
    videoView.start();
  }
}
